Emilie FIGUIE
============

-------------------     ----------------------------
e-mail                      e.collot@outlook.fr
telephone                   06 89 18 97 50
-------------------     ----------------------------

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/emifi/cv.git
```

Go to the project directory

```bash
  cd cv
```

double cliquer sur 
```bash
  index.html
```


![Screenshot](site.png)


